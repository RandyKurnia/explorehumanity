<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  $encodeMsg = "*Konfirmasi Transfer Donasi* ".urlencode("\r\n\r\n")."Nama : ".urlencode("\r\n").
          "Donasi : Rp. ".urlencode("\r\n")."Tangal Transfer : ".urlencode("\r\n")."Bukti Transfer : ".urlencode("\r\n");

  $waLink = "http://api.whatsapp.com/send?phone=6281229997667&text=".$encodeMsg;

?>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>explore! | Inspiring a better future</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <?php $this->load->view('shared/meta') ?>
    </head>    
    <body> 
    <?php $this->load->view('shared/header') ?>   
    <div class="space"></div>
    <div class="main-container">
      <div class="container">
        <h2>Panduan Umum</h2>
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
          <div class="panel-heading">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
              <h4 class="panel-title">              
                  Bagaimana cara berdonasi ?              
              </h4>
            </a>
          </div>
          <div id="collapse1" class="panel-collapse collapse in">
            <div class="panel-body">
              <ol>
                <li>
                  Donasi online di website explorehumanity.id.
                  <p>
                    Pilih program yang Anda minati >> Klik tombol Donasi  >> Masukkan informasi yang diminta (jumlah donasi, kontak Anda)
                    Di akhir proses donasi online, Anda akan memperoleh no rekening bank dan tagihan (nominal donasi + kode unik) yang diminta sistem.
                  </p>
                </li>

                <li>
                  Transfer dengan mencantumkan kode unik pada nominal transfer.
                  <p>
                    Mohon transfer tepat sesuai dengan nominal tagihan sistem (mencantumkan kode unik yang diberikan sistem), supaya donasi Anda terverifikasi secara otomatis oleh sistem.
                    Kode unik tersebut juga akan diakumulasikan sebagai donasi pada program yang Anda maksud.
                  </p>
                </li>
                <!-- <li>
                  Maksimal dalam 2 x 24 Jam, donasi akan diverifikasi oleh tim kami.
                </li> -->
              </ol>
            </div>
          </div>
        </div>        
        <div class="panel panel-default">
          <div class="panel-heading">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">  
              <h4 class="panel-title">              
                Apa yang akan terjadi apabila Saya lupa memasukkan kode unik pada nominal transfer ?              
              </h4>
            </a>
          </div>
          <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
              <p>
                Ketika Anda tidak mencantumkan <strong>kode unik</strong> pada saat transfer, maka donasi tidak akan terverifikasi.
                <br/>Jika ini terjadi, Anda wajib melakukan konfirmasi dengan cara mengirimkan bukti transfer melalui <a href="mailto:explorehumanity.id@gmail.com"><i class="fa fa-envelope"></i>  explorehumanity.id@gmail.com</a> atau 
                <br/>bisa melalui whatsapp ke nomor<a href="<?php echo $waLink; ?>"> 0812 2999 7667</a>
              </p>
            </div>
          </div>
        </div>
         <div class="panel panel-default">
          <div class="panel-heading">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
              <h4 class="panel-title">              
                  Apa yang akan terjadi apabila Saya lupa mencantumkan kode unik dan tidak melakukan konfirmasi ?             
              </h4>
            </a>
          </div>
          <div id="collapse3" class="panel-collapse collapse">
            <div class="panel-body">
              <p>
                Jika Anda melakukan transfer namun tidak mencantumkan <strong>kode unik</strong> atau mencantumkan <strong>kode unik</strong> yang salah dan Anda lupa melakukan konfirmasi maka dana donasi yang masuk akan Kami alokasikan kedalam salah satu program yang sedang berjalan.
              </p>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
              <h4 class="panel-title">              
                  Konfirmasi Donasi              
              </h4>
            </a>
          </div>
          <div id="collapse4" class="panel-collapse collapse">
            <div class="panel-body">
              <p>
                Jika Anda melakukan transfer namun tidak mencantumkan <strong>kode unik</strong> atau memasukkan <strong>kode unik</strong> yang salah, Anda <strong><u>harus melakukan konfirmasi.</u></strong>  
                Konfirmasi dapat dilakukan dengan mengirimkan bukti transfer ke email
                <a href="mailto:explorehumanity.id@gmail.com"><i class="fa fa-envelope"></i>  explorehumanity.id@gmail.com</a> / 
                bisa melalui whatsapp ke nomor<a href="<?php echo $waLink; ?>">0812 2999 7667</a>
              </p>
            </div>
          </div>
        </div>   
        <div class="panel panel-default">
          <div class="panel-heading">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
              <h4 class="panel-title">              
                  Kesalahan yang sering dilakukan saat berdonasi              
              </h4>
            </a>
          </div>
          <div id="collapse5" class="panel-collapse collapse">
            <div class="panel-body">
              <ol>
                <li>
                  <strong>Transfer tanpa mencantumkan kode unik</strong>
                  <p>
                    Ketika Anda tidak mencantumkan kode unik pada saat transfer, maka donasi tidak akan terverifikasi.
                    Jika ini terjadi, Anda wajib melakukan konfirmasi.
                  </p>
                </li>
                <li>
                  <strong>Transfer dengan kode unik yang salah</strong>
                  <p>
                    Ketika Anda salah mencantumkan kode unik pada saat transfer, maka donasi tidak akan terverifikasi.
                    Jika ini terjadi, Anda wajib melakukan konfirmasi.
                  </p>
                </li>
                <li>
                  <strong>Transfer setelah melewati / terlalu mepet deadline</strong>
                  <p>
                    Kami sangat menyarankan agar Anda segera melakukan transfer setelah berdonasi online. 
                    Transfer terlalu dekat dengan deadline meningkatkan kemungkinan donasi Anda tidak terverifikasi. 
                    Jika ini terjadi pada Anda, Anda wajib melakukan konfirmasi.
                  </p>
                </li>
                <li>
                  <strong>Menggabungkan donasi menjadi satu kali transfer</strong>
                  <p>
                    Apabila Anda berdonasi di lebih dari satu program, mohon untuk tidak menggabungkan nominal donasi pada satu kali transfer. 
                    <br/>
                    Sebagai contoh, apabila Anda berdonasi ke program A sebesar Rp. 50.300 dan ke campaign B sebesar Rp. 50.500, mohon di transfer satu per satu dan tidak digabungkan menjadi Rp. 100.800 karena sistem kami tidak akan mengenali transfer tersebut. 
                  </p>
                </li>
              </ol>
            </div>
          </div>
        </div>    
      </div>
    </div>        
    <?php $this->load->view('shared/footer') ?>
    </body>    
</html>