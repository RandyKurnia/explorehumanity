<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<html class="no-js">
    
    <head>

        <meta charset="utf-8">

        <title>explore! | Inspiring a better future</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">        

        <?php $this->load->view('shared/meta') ?>

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Dosis:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/login.css">

        <!-- Bootsrap -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/inspinia/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/inspinia/style.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/inspinia/animate.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/inspinia/font-awesome.min.css">

        <!-- Kendo -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/kendo/kendo.common-bootstrap.min.css"> 
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/kendo/kendo.bootstrap.min.css">

    </head>
    <body class="login-bg">
        <div class="loginscreen middle-box animated fadeInDown">
            <div class="text-center">
                <img src="<?php echo base_url() ?>assets/images/logo-white.png"  />
                <h3>Welcome to explore! Management System</h3>
            </div>
            <form action="<?php echo base_url();?>admin/authentication" method="post" enctype="multipart/form-data" class="form-horizontal" id="loginForm">
                <fieldset>
                    <legend>Masuk Ke Dalam Aplikasi</legend>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="text" class="form-control" id="Username" name="Username" placeholder="Alamat Email" required data-required-msg="Username harus diisi" />       
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" id="Password" name="Password" placeholder="Password" required data-required-msg="Password harus diisi"/>        
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 text-danger">
                            <?php echo $this->session->flashdata('message');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary automatic-width">Login</button>
                        </div>                        
                    </div>         
                    <div class="form-group">
                        <div class="col-xs-12">
                            <a href="#" class="text-warning">Lupa Password</a>
                        </div>
                    </div>             
                </fieldset>
            </form>
        </div>
    </body>

    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-migrate-3.0.0.min.js"></script>

    <!-- kendo -->
    <script src="<?php echo base_url();?>assets/js/kendo/kendo.all.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/kendo/cultures/kendo.culture.id-ID.min.js"></script>
    <script type="text/javascript">
        $(function(){
            var validator = $(".form-horizontal").kendoValidator().data("kendoValidator");
            
        });
    </script>
</html>