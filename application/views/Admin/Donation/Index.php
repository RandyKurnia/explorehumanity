<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html class="no-js">

    <head>

        <meta charset="utf-8">

        <title>explore! | Donation</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">        

        <?php $this->load->view('shared/Admin/LoadScript') ?>

    </head>

    <body>
    	<!-- Wrapper-->
    	<div id="wrapper">
    		 <!-- Navigation -->
    		<?php $this->load->view('shared/Admin/Navigation') ?>

    		 <!-- Page wraper -->
	        <div id="page-wrapper" class="gray-bg">

	            <!-- Top Navbar -->
	            <?php $this->load->view('shared/Admin/TopNavbar') ?>
                <div class="wrapper wrapper-content">                                   
                    <div class="row">                        
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>List Donasi </h5>
                                </div>
                                <div class="ibox-content">
                                        <div class="row">                                        
                                            <div class="col-lg-3">
                                                <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                                <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                                            </div>
                                        </div>
                                        <div id="grid">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	            
                <!-- Footer -->
	            <?php $this->load->view('shared/Admin/Footer') ?>
	        </div>
		</div>
    </body>      
</html>
<script type="text/javascript">
    
    $(function(){

        var urlx = "<?php echo base_url().'admin/donation/getall';?>";
        var urlBank = "<?php echo base_url().'admin/donation/getallbank';?>";
        var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        type: 'post',
                        url: urlx,   
                        dataType: 'json'
                    }
                },
                error: function(e) {
                    console.log(e);
                },
                schema: {
                    data: function(data){
                        return data.Result;
                    },
                    total: function(data){
                        return data.Count;
                    },
                    model: {
                        fields: {
                            Id:{ type: "number" },
                            ProgramId: { type: "number" },
                            Total: { type: "number" },
                            DonationDate: { type: "date" },
                            ExpireDate: { type: "date" },
                            // DonaturId: { type: "number" },
                            // CreateBy: { type: "number" },
                            // ConfirmationBy: { type: "number" },
                            // DonationCategoryId: { type: "number" },
                            // SourceId: { type: "number" },
                            // Amount: { type: "number" },
                            // UniqueCode: { type: "number" },
                            // Total: { type: "number" },
                            // Status: { type: "number" },                            
                            
                            // ConfirmationDate: { type: "date" },
                            
                        }
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
        });

        $("#grid").kendoGrid({
            dataSource: ds,
            filterable: kendoGridFilterable,
            scrollable: true,
            sortable: true,
            pageable: true,
            height:"460px",
            columns: [                                            
                {
                    field:"Total",
                    title:"Nominal",
                    template:"<div class=\"text-right\">Rp. #:kendo.format(\"{0:N0}\",Total)#</div>",
                    width:"150"                   
                },
                {
                    field:"Bank",
                    title:"Bank",                    
                    width:"150",
                    filterable: dropDownListFilterable(bankOptionFilter, urlBank),
                },
                {
                    field:"AccountNo",
                    title:"Rek.",
                    width:"180"                    
                },
                {
                    field:"DonaturName",
                    title:"Donatur",                    
                    width:"250"
                },
                {
                    field:"DonaturEmail",
                    title:"Email",   
                    template: "<a href=\"mailto:#:DonaturEmail#\"><i class=\"fa fa-envelope\"></i> #:DonaturEmail#</a>",
                    width:"250"
                },
                {
                    field:"DonaturPhone",
                    title:"Telp.",
                    width:"150"                    
                },
                {
                    field:"ProgramName",
                    title:"Campaign",                    
                    width:"250"
                }, 
                {
                    field:"ProgramCategoryName",
                    title:"Kategori Program",                    
                    width:"200"
                }, 
                {
                    field:"DonationDate",
                    title:"Tanggal Donasi",  
                    format:"{0:d MMMM yyyy (HH:mm)}",
                    filterable:rangeDateFilterable,
                    width:"200"                  
                },
                {
                    field:"ExpireDate",
                    title:"Masa Tenggang",  
                    format:"{0:d MMMM yyyy (HH:mm)}",
                    filterable:rangeDateFilterable,
                    width:"200"                  
                },
                {
                    field:"DonationCategoryName",
                    title:"Kategori Donasi",                      
                    width:"180"
                },
                {
                    field:"StatusDescription",
                    title:"Status",                      
                    width:"250"
                },
                // {
                //     field:"DonaturId",
                //     title:"Donatur",                    
                // },
                // {
                //     field:"Amount",
                //     title:"Nominal",                    
                //     format:"{0:N0}",
                //     template:"<p class=\"text-right\">#:kendo.format(\"{0:N0}\",Amount)#</p>"
                // },
                // {
                //     field:"UniqueCode",
                //     title:"Kode Unik",                    
                // },
                // {
                //     field:"Total",
                //     title:"Total",    
                //     format:"{0:N0}"                
                // },
                
                // {
                //     field:"DonationCategoryId",
                //     title:"Kategori",                    
                // },
                // {
                //     field:"SourceId",
                //     title:"Sumber Informasi",                    
                // },
                // {
                //     field:"CreateBy",
                //     title:"PIC",                    
                // },
                // {
                //     field:"ConfirmationDate",
                //     title:"Tanggal Konfirmasi",                    
                //     filterable:rangeDateFilterable                  
                // },
                // {
                //     field:"ConfirmationBy",
                //     title:"Di konfirmasi oleh",                    
                // },
            ]
        });
    });


</script> 