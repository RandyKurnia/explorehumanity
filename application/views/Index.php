<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>explore! | Inspiring a better future</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <?php $this->load->view('shared/meta') ?>
       <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '2031833486859499');
          fbq('init', '2233649773366435');
          fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2031833486859499&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
    </head>
    <body>
    <?php $this->load->view('shared/header') ?>
         <!-- Carousel
        ================================================== -->
        <div id="homeCarousel" class="carousel slide carousel-home" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php foreach ($program as $key => $value) { 
                  if($key == 0){
                    $active = "active";
                  }else{
                    $active = "";
                  }
                ?>
                <li data-target="#homeCarousel" data-slide-to="<?php echo $key; ?>" class="<?php echo $active ?>"></li>
                <?php } ?>
            </ol>

            <div class="carousel-inner" role="listbox">
                <?php foreach ($program as $key => $value) {           
                      if($key == 0){
                        $active = "active";
                      }else{
                        $active = "";
                      }
                    ?>
                    <div class="item <?php echo $active; ?>">
                        <?php 
                            $namalink= preg_replace("/[^a-zA-Z0-9\s]/","", $value->Name);
                            $namalink = str_replace(" ","-",$namalink);
                            $namalink = strtolower($namalink);
                        ?>
                        <a href="<?php echo base_url();?>donation/detail/<?php echo $namalink.'_'.$value->Id; ?>.html">                
                            <img src="<?php echo base_url();?>assets/images/banner/<?php echo $value->Banner; ?>" alt="<?php echo $value->Name; ?>" class="lazyload bounceInDown animated slow img-responsive"/>            
                        </a>  
                    </div>
                <?php } ?>
            </div>
      
            <a class="left carousel-control" href="#homeCarousel" role="button" data-slide="prev">
                <span class="fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="right carousel-control" href="#homeCarousel" role="button" data-slide="next">
                <span class="fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <!-- separator -->
        <!-- <div class="footer-top"></div> -->
        
        <?php $this->load->view('shared/program') ?>
        <div class="space"></div>
    
    <!-- separator -->
    <!-- <div class="footer-top"></div> -->
    <?php $this->load->view('shared/footer') ?>
    </body>    
    <!-- WhatsHelp.io widget -->
    <script type="text/javascript">
        (function () {
            var options = {
                whatsapp: "+6281229997667", // WhatsApp number
                call_to_action: "Hubungi Kami", // Call to action
                position: "right", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /WhatsHelp.io widget -->

    <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a03ccb05b14cc7c"></script>  -->
</html>