<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

?>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>explore! | Inspiring a better future</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <?php $this->load->view('shared/meta') ?>
    </head>    
    <body> 
      <?php $this->load->view('shared/header') ?>       
      <div class="main-container">
        <div class="page-heading text-center">
        <div class="container zoomIn animated">      
          <h1 class="page-title">PROFIL <span class="title-under"></span></h1>
          <p class="page-description">
            explore! inspiring a better future
          </p>      
        </div>
      </div>
      <div class="container">
        <div class="row fadeIn animated">
          <div class="col-md-6">
            <img src="assets/images/about-us.jpg" alt="" class="img-responsive">
          </div>
          <div class="col-md-6">
            <h2 class="title-style-2">Tentang Kami <span class="title-under"></span></h2>
            <p>
              Pada tanggal 2 Mei 2018 explore! dibentuk secara resmi sebagai lembaga kemanusiaan internasional. Lembaga ini berkomitmen untuk memberikan bantuan nyata di wilayah-wilayah yang dilanda krisis kemanusiaan ataupun wilayah-wilayah yang terkena bencana alam, baik yang terjadi di dalam maupun luar negeri. Luputnya perhatian publik terhadap krisis-krisis kemanusiaan yang terjadi di dunia luar menjadikan explore! tergerak untuk menjadi salah satu lembaga kemanusiaan yang bisa menjadi sarana informasi sekaligus menyalurkan bantuan di wilayah-wilayah yang terdampak krisis kemanusiaan.
            </p>
            <p>
             Oleh karena itu, explore! hadir untuk memberikan pencerahan pada masyarakat luas mengenai kondisi saudara-saudara kita di luar sana dengan menyajikan ragam program yang “renyah” untuk dinikmati oleh masyarakat pada umumnya.
            </p>
          </div>
        </div> <!-- /.row -->
        <div class="row fadeIn animated">
          <div class="col-md-12">
            <h2 class="title-style-2">Visi <span class="title-under"></span></h2>
            <p>
              Mengembalikan harapan dan senyuman ceria saudara-saudara kita yang terdampak krisis kemanusiaan atau bencana alam lewat distribusi bantuan yang dapat memenuhi kebutuhan-kebutuhan dasar mereka.
            </p>            
          </div>
        </div> <!-- /.row -->
        <div class="row fadeIn animated">
          <div class="col-md-12">
            <h2 class="title-style-2">Misi <span class="title-under"></span></h2>
            <p>
              <ol>
                <li>
                  Mengedukasi masyarakat tentang krisis kemanusiaan yang terjadi di dalam maupun luar negeri dengan menjalin kerjasama dengan sebanyak mungkin elemen masyarakat.
                </li>
                <li>
                  Menjadi penggerak tumbuhnya rasa empati masyarakat terhadap krisis kemanusiaan sehingga mereka mau berkontribusi secara nyata.
                </li>
              </ol>

            </p>            
          </div>
        </div> <!-- /.row -->
        <div class="row fadeIn animated">
          <div class="col-md-12">
            <h2 class="title-style-2">Legal Formal <span class="title-under"></span></h2>
          </div>
          <div class="col-md-6">            
            <p>
              <ul>
                <li>
                  Akta Notaris
                  <p>
                    Yayasan Explore Indonesia<br/>
                    Yukasanu Santihapsari, S.H., M.Kn.<br/>
                    No. 1 tanggal 02 Mei 2018<br/>
                    Kab. Bandung Barat
                  </p>
                </li>                
              </ul>
            </p>            
          </div>
          <div class="col-md-6">            
            <p>
              <ul>
                <li>
                  Menteri Hukum dan HAM
                  <p>                    
                    AHU-0008857.AH.01.12. Tahun 2018
                  </p>
                </li>                
              </ul>
            </p>            
          </div>
        </div> <!-- /.row -->
        <!-- <div class="section-home about-us">
          <div class="row">
            <div class="col-md-3 col-sm-6">
              <div class="about-us-col">
                  <div class="col-icon-wrapper">
                    <img src="assets/images/icons/our-mission-icon.png" alt="">
                  </div>
                  <h3 class="col-title">our mission</h3>
                  <div class="col-details">
                    <p>Lorem ipsum dolor sit amet consect adipisscin elit proin vel lectus ut eta esami vera dolor sit amet consect</p>                        
                  </div>
                  <a href="#" class="btn btn-primary"> Read more </a>                        
                </div>        
              </div>
              <div class="col-md-3 col-sm-6">            
                <div class="about-us-col">
                  <div class="col-icon-wrapper">
                    <img src="assets/images/icons/make-donation-icon.png" alt="">
                  </div>
                  <h3 class="col-title">Make donations</h3>
                  <div class="col-details">
                    <p>Lorem ipsum dolor sit amet consect adipisscin elit proin vel lectus ut eta esami vera dolor sit amet consect</p>                            
                  </div>
                  <a href="#" class="btn btn-primary"> Read more </a>
                </div>                    
              </div>
              <div class="col-md-3 col-sm-6">                 
                <div class="about-us-col">
                  <div class="col-icon-wrapper">
                    <img src="assets/images/icons/help-icon.png" alt="">
                  </div>
                  <h3 class="col-title">Help & support</h3>
                  <div class="col-details">
                    <p>Lorem ipsum dolor sit amet consect adipisscin elit proin vel lectus ut eta esami vera dolor sit amet consect</p>                            
                  </div>
                  <a href="#" class="btn btn-primary"> Read more </a>                    
                </div>                    
              </div>
              <div class="col-md-3 col-sm-6">                
                <div class="about-us-col">
                  <div class="col-icon-wrapper">
                    <img src="assets/images/icons/programs-icon.png" alt="">
                  </div>
                  <h3 class="col-title">our programs</h3>
                  <div class="col-details">
                    <p>Lorem ipsum dolor sit amet consect adipisscin elit proin vel lectus ut eta esami vera dolor sit amet consect</p>                              
                  </div>
                  <a href="#" class="btn btn-primary"> Read more </a>                    
                </div>                    
              </div>
            </div> 
          </div> 
        </div> -->
        <div class="space"></div>
        <div class="our-team fadeIn animated">
          <div class="container"> 
            <h2 class="title-style-1">Struktur Organisasi<span class="title-under"></span></h2>
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="team-member">
                  <div class="thumnail">
                    <img src="assets/images/team/member-1.jpg" alt="" class="cause-img">                            
                  </div>
                  <h4 class="member-name">Ridhan Hafiedz, S.H., S.T</h4>
                  <div class="member-position">
                    DIREKTUR
                  </div>
                   <!--  <div class="btn-holder">
                      <a href="#" class="btn"> <i class="fa fa-envelope"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-facebook"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-google"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-twitter"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-linkedin"></i> </a>                            
                    </div> -->                            
                </div> <!-- /.team-member -->                      
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="team-member">
                  <div class="thumnail">
                    <img src="assets/images/team/member-1.jpg" alt="" class="cause-img">              
                  </div>
                  <h4 class="member-name">Ferdi Yusuf, S.Kom</h4>
                  <div class="member-position">
                    SEKRETARIS
                  </div>
                  <!-- <div class="btn-holder">
                    <a href="#" class="btn"> <i class="fa fa-envelope"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-facebook"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-google"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-twitter"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-linkedin"></i> </a>                
                  </div> -->                          
                </div> <!-- /.team-member -->            
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="team-member">
                  <div class="thumnail">
                    <img src="assets/images/team/member-1.jpg" alt="" class="cause-img">                
                  </div>
                  <h4 class="member-name">Rizky Priatna, S.E</h4>
                  <div class="member-position">
                    MANAJER KEUANGAN
                  </div>
                  <!-- <div class="btn-holder">
                    <a href="#" class="btn"> <i class="fa fa-envelope"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-facebook"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-google"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-twitter"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-linkedin"></i> </a>                  
                  </div> -->
                </div> <!-- /.team-member -->
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="team-member">
                  <div class="thumnail">
                    <img src="assets/images/team/member-1.jpg" alt="" class="cause-img">
                  </div>
                  <h4 class="member-name">Anggara Budhi P, A.Md</h4>
                  <div class="member-position">
                    MANAJER PROGRAM
                  </div>
                  <!-- <div class="btn-holder">
                    <a href="#" class="btn"> <i class="fa fa-envelope"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-facebook"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-google"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-twitter"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-linkedin"></i> </a>                
                  </div> -->  
                </div> <!-- /.team-member -->
              </div>
            </div> <!-- /.row -->          
            <div class="space"></div>
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="team-member">
                  <div class="thumnail">
                    <img src="assets/images/team/member-1.jpg" alt="" class="cause-img">                            
                  </div>
                  <h4 class="member-name">Sello Adi Putra, S.T</h4>
                  <div class="member-position">
                    MANAJER DIVISI MEDIA
                  </div>
                   <!--  <div class="btn-holder">
                      <a href="#" class="btn"> <i class="fa fa-envelope"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-facebook"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-google"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-twitter"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-linkedin"></i> </a>                            
                    </div> -->                            
                </div> <!-- /.team-member -->                      
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="team-member">
                  <div class="thumnail">
                    <img src="assets/images/team/member-1.jpg" alt="" class="cause-img">              
                  </div>
                  <h4 class="member-name">Deni Wahyudi</h4>
                  <div class="member-position">
                    MANAJER FUNDRAISING
                  </div>
                  <!-- <div class="btn-holder">
                    <a href="#" class="btn"> <i class="fa fa-envelope"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-facebook"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-google"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-twitter"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-linkedin"></i> </a>                
                  </div> -->                          
                </div> <!-- /.team-member -->            
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="team-member">
                  <div class="thumnail">
                    <img src="assets/images/team/member-1.jpg" alt="" class="cause-img">                
                  </div>
                  <h4 class="member-name">Rizki Fahdli</h4>
                  <div class="member-position">
                    MANAJER DIVISI KREATIF
                  </div>
                  <!-- <div class="btn-holder">
                    <a href="#" class="btn"> <i class="fa fa-envelope"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-facebook"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-google"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-twitter"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-linkedin"></i> </a>                  
                  </div> -->
                </div> <!-- /.team-member -->
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="team-member">
                  <div class="thumnail">
                    <img src="assets/images/team/member-1.jpg" alt="" class="cause-img">
                  </div>
                  <h4 class="member-name">Tatang Kosasih</h4>
                  <div class="member-position">
                    MANAJER CRISIS CENTER
                  </div>
                  <!-- <div class="btn-holder">
                    <a href="#" class="btn"> <i class="fa fa-envelope"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-facebook"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-google"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-twitter"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-linkedin"></i> </a>                
                  </div> -->  
                </div> <!-- /.team-member -->
              </div>
            </div> <!-- /.row -->
            <div class="space"></div>
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="team-member">
                  <div class="thumnail">
                    <img src="assets/images/team/member-1.jpg" alt="" class="cause-img">                            
                  </div>
                  <h4 class="member-name">Zaky Mansur</h4>
                  <div class="member-position">
                    MANAJER DIVISI RUMAH TANGGA
                  </div>
                   <!--  <div class="btn-holder">
                      <a href="#" class="btn"> <i class="fa fa-envelope"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-facebook"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-google"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-twitter"></i> </a>
                      <a href="#" class="btn"> <i class="fa fa-linkedin"></i> </a>                            
                    </div> -->                            
                </div> <!-- /.team-member -->                      
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="team-member">
                  <div class="thumnail">
                    <img src="assets/images/team/member-1.jpg" alt="" class="cause-img">              
                  </div>
                  <h4 class="member-name">Satrio Utomo, S.E</h4>
                  <div class="member-position">
                    HUMAS
                  </div>
                  <!-- <div class="btn-holder">
                    <a href="#" class="btn"> <i class="fa fa-envelope"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-facebook"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-google"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-twitter"></i> </a>
                    <a href="#" class="btn"> <i class="fa fa-linkedin"></i> </a>                
                  </div> -->                          
                </div> <!-- /.team-member -->            
              </div>              
            </div> <!-- /.row -->
          </div>
        </div>
      </div>  
      <div class="space"></div>      
      <?php $this->load->view('shared/footer') ?>
    </body>    
</html>