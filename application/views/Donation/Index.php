    <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html class="no-js">
    <?php
        $namalink= preg_replace("/[^a-zA-Z0-9\s]/","", $program->Name);
        $namalink = str_replace(" ","-",$namalink);
        $namalink = strtolower($namalink);                          
    ?>  
    <head>
        <meta charset="utf-8">
        <title>explore! | Inspiring a better future</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:type" content="article" />
        <meta name="description" content="<?php echo strip_tags($program->Description); ?>">
        <meta name="twitter:card" value="<?php echo strip_tags($program->Description); ?>">
        <meta property="og:title" content="<?php echo $program->Name; ?>" />
        <meta property="og:url" content="http://explorehumanity.id/donation/detail/<?php echo $namalink.'_'.$program->Id; ?>.html" />
        <meta property="og:image" content="http://explorehumanity.id/assets/images/photo/<?php echo $program->Photo; ?>" />
        <meta property="og:description" content="<?php echo strip_tags($program->Description); ?>" />
        <meta property="og:image:alt" content="<?php echo $program->Name;?>" />
        <link href="<?php echo base_url();?>assets/css/up.css" rel="stylesheet">    

         <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '2031833486859499');
          fbq('init', '2233649773366435');
          fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2031833486859499&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
    </head>
    <body>
    <?php $this->load->view('shared/header') ?>
        <div class="main-container">
            <div class="container">
                <h2 class="title-style-1"> <?php echo $program->Name; ?> <span class="title-under"></span></h2>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="btn-holder text-center hidden-lg hidden-md" >
                          <a href="<?php echo base_url();?>donation/program/<?php echo $namalink.'_'.$program->Id; ?>.html" class="btn btn-danger" style="width: 100%;"> DONASI SEKARANG </a>                     
                          <br/>
                          <br/>
                        </div>
                        <img src="<?php echo base_url();?>assets/images/photo/<?php echo $program->Photo; ?>" alt="<?php echo $program->Name; ?>" class="lazyload bounceInDown animated slow img-responsive"><br>
                    </div>
                    <div class="col-lg-4">
                        <?php echo $program->Description ?>
                    </div>
                    <div class="col-lg-4">
                        <div class="btn-holder text-center hidden-xs" >
                          <a href="<?php echo base_url();?>donation/program/<?php echo $namalink.'_'.$program->Id; ?>.html" class="btn btn-danger" style="width: 100%;"> DONASI SEKARANG </a>                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php $this->load->view('shared/footer') ?>     
    <a href="<?php echo base_url();?>donation/program/<?php echo $namalink.'_'.$program->Id; ?>.html" class="hidden-lg hidden-md">   
        <button class="btn btn-danger scroll-top-wrapper" >DONASI SEKARANG</button>
    </a>
    </body>   
    <script type="text/javascript">
        $(document).ready(function(){
            $(function(){
                $(document).on('scroll', function(){
             
                    if ($(window).scrollTop() > 100) {
                        $('.scroll-top-wrapper').addClass('show');
                    } else {
                        $('.scroll-top-wrapper').removeClass('show');
                    }
                });
            });
        });

    </script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a03ccb05b14cc7c"></script> 
</html>
