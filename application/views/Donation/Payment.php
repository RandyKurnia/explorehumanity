<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html class="no-js">

    <head>

        <meta charset="utf-8">

        <title>explore! | Inspiring a better future</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">        

        <?php $this->load->view('shared/meta') ?>
        <?php 
            $namalink= preg_replace("/[^a-zA-Z0-9\s]/","", $currentProgram->Name);
            $namalink = str_replace(" ","-",$namalink);
            $namalink = strtolower($namalink);

            $urlLink = base_url()."donation/program/".$namalink.'_'.$currentProgram->Id.".html";
        ?>
          <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '2031833486859499');
          fbq('init', '2233649773366435');
          fbq('track', 'Donate');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2031833486859499&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
    </head>



    <body>
    <?php $this->load->view('shared/header') ?>
    <div class="main-container">
        <div class="space"></div>
        <div class="row">            
            <div class="col-md-4 col-md-offset-4 col-xs-12">     
                <div class="panel panel-primary">
                    <div class="panel-body">                            
                        <div class="row text-center">           
                            <h4>Instruksi Pembayaran</h4>   
                            <p>Transfer tepat sesuai nominal berikut</p>                     
                        </div>                                   
                        <div class="row">          
                            <div class="col-xs-1"></div> 
                            <div class="col-xs-10 text-center">                            
                                <h2><strong>Rp. <?php echo number_format($donation->Total,0,".","."); ?></strong></h2>
                            </div>         
                            <div class="col-xs-1"></div>
                        </div>      
                        <div class="row">        
                            <div class="col-xs-1"></div>    
                            <div class="col-xs-10 text-center">                        
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <p>
                                            <small><strong>Penting!</strong></small>
                                            <small>Mohon transfer tepat sampai 3 angka terakhir agar donasi Anda dapat diproses.</small>
                                        </p>                        
                                    </div>
                                </div>
                            </div>                      
                            <div class="col-xs-1"></div> 
                        </div>  
                        <div class="row">         
                            <div class="col-xs-1"></div>   
                            <div class="col-xs-10 text-center">    
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <p class="text-left"><strong>Jumlah <span class="text-right pull-right">Rp. <?php echo number_format($donation->Amount,0,".","."); ?></span></strong></p>     
                                        <?php if($currentProgram->UniqueCode > 0) { ?>
                                            <p class="text-left"><strong>Kode Unik (*) <span class="text-right pull-right">Rp. <?php echo number_format($currentProgram->UniqueCode,0,".","."); ?></span></strong></p>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1"></div> 
                        </div> 
                        <div class="row">         
                            <div class="col-xs-1"></div>   
                            <div class="col-xs-10 text-left">                  
                                <p><small><em>* 3 angka terakhir akan didonasikan</em></small></p>                                                     
                            </div>
                            <div class="col-xs-1"></div> 
                        </div> 
                    </div>                
                </div>       
            </div>      
        </div> 
        <div class="row">            
            <div class="col-md-4 col-md-offset-4 col-xs-12">     
                <div class="panel panel-primary">
                    <div class="panel-body">                                                         
                        <div class="row text-center">           
                            <p>Transfer ke rekening<br>Atas Nama <strong><?php echo $bank->AccountName ?></strong><br></p>
                            <div class="col-xs-1"></div> 
                            <div class="col-xs-10 text-center">                            
                                <!-- <div class="panel panel-default">
                                    <div class="panel-body"> -->
                                        <div class="row">
                                        <div class="col-xs-4">
                                            <h4>
                                            <img src="<?php echo base_url();?>assets/images/logo/<?php echo $bank->Logo; ?>" alt="bni syariah" class="logo-img">
                                            </h4>
                                        </div>
                                        <div class="col-xs-8">
                                            <h4><strong><?php echo $bank->AccountNo; ?></strong><br/><small>
                                            <?php echo $bank->Branch; ?></small></h4>
                                        </div>   
                                        </div>
                                    <!-- </div>
                                </div> -->
                            </div>           
                            <div class="col-xs-1"></div>            
                        </div>  
                        <!-- <div class="row">         
                            <div class="col-xs-1"></div>   
                            <div class="col-xs-10 text-center">    
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <p>
                                            <small>Transfer sebelum tanggal <strong><?php echo date_format(new datetime($donation->ExpireDate), "d M Y H:i:s"); ?></strong> atau donasi Anda otomatis dibatalkan oleh sistem.</small>
                                        </p>                        
                                    </div>
                                </div>
                            </div>                      
                            <div class="col-xs-1"></div> 
                        </div>  -->                        
                    </div>                
                </div>       
            </div>      
        </div>  
        <div class="row">            
            <div class="col-md-4 col-md-offset-4 col-xs-12">                                                             
                <div class="row text-center">                              
                    <div class="col-xs-1"></div> 
                    <div class="col-xs-10 text-center">                            
                       <h4>Progam-program lain juga butuh bantuan Anda, lihat selengkapnya!</h4>
                    </div>           
                    <div class="col-xs-1"></div>            
                </div>      
                <div class="row text-center">   
                <?php foreach ($program as $key => $value) { ?>     
                                            
                    
                    <div class="col-xs-6 text-center">      
                        <a href="<?php echo base_url();?>donation/detail/<?php echo $namalink.'_'.$value->Id; ?>.html">
                            <img src="<?php echo base_url();?>assets/images/banner/<?php echo $value->Banner; ?>" alt="<?php echo $value->Name; ?>" class="lazyload slow automatic-width program-img img-responsive">                          
                        </a>
                        <div class="space"></div>    
                    </div>           
                                         
                
                <?php }?>
                </div>  
                                        
            </div>      
        </div>   
    </div> 
    <?php $this->load->view('shared/footer') ?> 
    </body>    
    <!-- WhatsHelp.io widget -->
    <script type="text/javascript">
        (function () {
            var options = {
                whatsapp: "+6281229997667", // WhatsApp number
                call_to_action: "Hubungi Kami", // Call to action
                position: "right", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /WhatsHelp.io widget -->
</html>