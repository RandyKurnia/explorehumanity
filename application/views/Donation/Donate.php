<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html class="no-js">   
    <head>
         <!-- Kendo -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/kendo/kendo.common-bootstrap.min.css"> 
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/kendo/kendo.bootstrap.min.css">
        <meta charset="utf-8">
        <title>explore! | Inspiring a better future</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <?php $this->load->view('shared/meta') ?>

         <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '2031833486859499');
          fbq('init', '2233649773366435');
          fbq('track', 'InitiateCheckout');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2031833486859499&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
    </head>
    <body>
    <?php $this->load->view('shared/header') ?>
    <div class="main-container">
        <div class="space"></div>
        <div class="container">
            <div class="col-md-6 col-md-offset-3 col-xs-12">     
                <div class="row text-center">           
                    <p>Anda akan berdonasi untuk program</p>
                    <h4><?php echo $program->Name; ?></h4>  
                </div>
                <div class="space"></div>
                <div class="row">
                     <form action="<?php echo base_url();?>donation/add" method="post" enctype="multipart/form-data" class="form-horizontal" id="donationForm">
                        <input type="hidden" id="ProgramId" name="ProgramId" value="<?php echo $program->Id; ?>">
                        <input type="hidden" id="UniqueCode" name="UniqueCode" value="<?php echo $program->UniqueCode; ?>">
                        <input type="hidden" id="ProgramName" name="ProgramName" value="<?php echo $program->Name; ?>">

                        <input type="hidden" id="Status" name="Status" value="1">                        
                        <input type="hidden" id="DonationCategoryId" name="DonationCategoryId" value="1">                  

                        <!-- set default -->
                        <!-- edit bagian ini jika user bisa memilih rekening -->
                        <input type="hidden" id="BankId" name="BankId" value="<?php echo $bankAccount->BankId; ?>">                  

                        <div class="form-group">                        
                            <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1">
                                <label for="Amount" class="text-left control-label">Jumlah Donasi (Rp.)</label>  
                                <div class="input-group">
                                    <span class="input-group-addon">Rp.</span>                              
                                    <input type="number" class="form-control-numeric" style="width:100%;" required data-required-msg="Jumlah Donasi harus diisi" id="Amount" name="Amount" placeholder="0">                                     
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">                        
                            <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1">
                                <label for="Name" class="text-left">Nama Lengkap</label>                                                                  
                                <input type="text" class="form-control" id="Name" name="Name" style="width: 100%;" required data-required-msg="Nama Lengkap harus diisi" placeholder="Nama Lengkap">
                                                                             
                            </div>
                        </div>
                        <div class="form-group">                        
                            <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1">
                                <label for="Email" class="text-left">Alamat Email</label>                                                                   
                                <input type="email" class="form-control" id="Email" name="Email" style="width: 100%;" data-required-msg="Alamat Email harus diisi" data-email-msg="Email format is not valid" placeholder="Alamat Email" required>  
                                                                                      
                            </div>
                        </div>
                        <div class="form-group">                        
                            <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1">
                                <label for="Phone" class="text-left">No. Telp.</label>                                                                   
                                <input type="number" class="form-control automatic-width numeric" id="Phone" name="Phone" required data-required-msg="No. Telp. harus diisi" placeholder="Format 08xxxxxxxxxx atau 628xxx (angka saja)">  
                                                                                                   
                            </div>
                        </div>
                        <div class="form-group">                        
                            <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1">
                                <label for="Phone" class="text-left">Sumber Informasi</label><br/>                                
                                Darimana Anda mendapatkan informasi ini :
                                <br/>
                                <?php
                                    foreach ($sourceInfo as $key => $value) { 
                                ?>
                                <input type="radio" name="SourceIdRadio" value="<?php echo $value->Id; ?>">
                                <?php echo $value->SourceName; ?>
                                <br/>
                                <?php                                          
                                    }
                                ?>
                                <input type="text" class="hide" id="SourceId" name="SourceId" required data-required-msg="Sumber Informasi harus diisi">                                
                                <input type="text" name="SourceOther" class="form-control hide" style="width: 100%;" data-required-msg="Sumber harus diisi">   
                                                         
                            </div>
                        </div>                       
                        <div class="form-group">                        
                            <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1">
                                 <?php 
                                    $namalink= preg_replace("/[^a-zA-Z0-9\s]/","", $program->Name);
                                    $namalink = str_replace(" ","-",$namalink);
                                    $namalink = strtolower($namalink);
                                ?>                                
                                <button type="submit" id="submitDonasi" class="btn btn-danger" style="width: 100%"> DONASI</button>                                                                    
                            </div>
                        </div>
                    </form>    
                </div>    
            </div>
        </div>      
    </div>
    <?php $this->load->view('shared/footer') ?>
    </body>
    

    <script type="text/javascript">
        $(function(){
            var validator = $(".form-horizontal").kendoValidator().data("kendoValidator");
            var form = $("#donationForm");
            var source = $("input[name='SourceIdRadio']");
            var sourceOther = $("input[name='SourceOther']");
            var sourceId = $("input[name='SourceId']");
            source.change(function(){
                var radioValue = $("input[name='SourceIdRadio']:checked").val();   
                   
                $(sourceId).val(radioValue);
                $(sourceOther).val("");                

                if(radioValue == 9){
                    $(sourceOther).addClass("show").removeClass("hide");
                    $(sourceOther).attr("required", true);
                }
                else{
                    $(sourceOther).addClass("hide").removeClass("show");   
                    $(sourceOther).attr("required", false);
                }
            });
            
            // $("#submitDonasi").click(function(e){               
            //    form.submit();
            // });
        });
    </script>


     <!-- kendo -->
    <script src="<?php echo base_url();?>assets/js/kendo/kendo.all.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/kendo/cultures/kendo.culture.id-ID.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/webapp.js"></script>

    <!-- WhatsHelp.io widget -->
    <!-- <script type="text/javascript">
        (function () {
            var options = {
                whatsapp: "+6281229997667", // WhatsApp number
                call_to_action: "Hubungi Kami", // Call to action
                position: "right", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script> -->
</html>