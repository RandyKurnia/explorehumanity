 <footer class="main-footer">
        <div class="footer-top">     
        </div>
        <div class="footer-main">
            <div class="container">
                <div class="row">                                         
                    <div class="col-md-4">
                        <div class="footer-col">
                            <p class="footer-title">KANTOR</span></p>
                            <div class="footer-content"> 
                                <table>
                                    <tr>
                                        <td>
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.6713568549962!2d107.62095421537539!3d-6.929828269758932!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e9e19ed5df1d%3A0xbc5a09589010c48f!2sExplore*21+Humanity!5e0!3m2!1sid!2sid!4v1553058235607" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                                            <br/><br/>
                                            <p><a href="https://goo.gl/maps/1uh8R4oHFRH2" target="_blank" class="footer-link"> <i class="fa  fa-map-marker m-r-2"></i>  Jl.Engkol No. 1A, Lengkong-Bandung, 40263</a></p>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td> <p><a href="tel:6281229997667" class="footer-link"><i class="fa fa-phone"></i>  +62 812 2999 7667 </a></p></td>
                                    </tr>
                                    <tr>
                                        <td><a href="mailto:explorehumanity.id@gmail.com" class="footer-link"><i class="fa fa-envelope"></i>  explorehumanity.id@gmail.com</a></td>                                        
                                    </tr>
                                </table>  
                            </div>
                        </div>
                    </div>               
                    <div class="col-md-4">
                        <div class="footer-col">
                            <p class="footer-title">SOSIAL MEDIA</p>
                            <div class="footer-content">
                                <table>
                                    <tr>
                                        <td><a href="https://www.facebook.com/pg/explorehumanity.id" target="_blank" class="footer-link"><i class="fa fa-sm fa-socmed fa-facebook"></i>&nbsp;&nbsp;Facebook</a></td>
                                    </tr>
                                    <tr>                                        
                                        <td><a href="https://www.youtube.com/channel/UCrveRiip7zvgpy8fYRxPh9w" target="_blank" class="footer-link"><i class="fa fa-socmed fa-youtube"></i>&nbsp;&nbsp;Youtube</a></td>
                                    </tr>
                                    <tr>                                        
                                        <td><a href="http://instagram.com/explorehumanity.id" target="_blank" class="footer-link"><i class="fa fa-socmed fa-instagram"></i>&nbsp;&nbsp;Instagram</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>    
                    <div class="col-md-4">
                        <div class="footer-col">
                            <p class="footer-title">LEGALITAS</span></p>
                            <div class="footer-content">
                                <strong>Akta Notaris</strong><br>
                                Yayasan Explore Indonesia<br>
                                Yukasanu Santihapsari, S.H., M.Kn.<br>
                                No. 1 tanggal 02 Mei 2018<br>
                                Kab. Bandung Barat
                                <br><br>
                                <strong>Kementerian Hukum dan Ham</strong><br>
                                AHU-0008857.AH.01.12. Tahun 2018
                            </div>
                        </div>
                    </div>                    
                </div>
                <!--  <div class="row mobile-footer">
                    <a href="#" class="btn btn-primary no-rounded col-xs-12" data-toggle="modal" data-target="#">Konfirmasi</a>
                    <a href="#" class="btn btn-fixed no-rounded col-xs-12" data-toggle="modal" data-target="#">Donasi</a>              
                </div> -->
            </div>
        </div>
    </footer> <!-- main-footer -->
