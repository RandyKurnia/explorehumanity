    <!DOCTYPE html>
	<!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,700' rel='stylesheet' type='text/css'>

    <!-- Bootsrap -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap/bootstrap.min.css">

    <!-- Font awesome -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">

    <!-- Template main Css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Owl carousel -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">    

    <!-- PrettyPhoto -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/prettyPhoto.css">

    <!-- jquery ui -->
    <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css"> -->

    <!-- Modernizr -->
    <script src="<?php echo base_url();?>assets/js/modernizr-2.6.2.min.js"></script>   

    <header class="main-header">    
        <nav class="navbar navbar-static-top">
            <div class="navbar-main">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/assets/images/logo-explore.png" alt="explore!" class="lazyload img-responsive"></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse pull-right">
                        <ul class="nav navbar-nav">
                            <li><a class="<?php if($this->uri->segment(1)==""){echo 'is-active';}?>" href="<?php echo base_url();?>">DONASI</a></li>
                            <li><a class="<?php if($this->uri->segment(1)=="profile"){echo 'is-active';}?>" href="<?php echo base_url();?>profile">PROFIL</a></li>
                            <li><a class="<?php if($this->uri->segment(1)=="help"){echo 'is-active';}?>" href="<?php echo base_url();?>help" >BANTUAN</a></li>  
                            <!-- <li><a href="about.html">ABOUT</a></li> -->
                            <!-- <li class="has-child"><a href="#">CAUSES</a>

                              <ul class="submenu">
                                 <li class="submenu-item"><a href="causes.html">Causes list </a></li>
                                 <li class="submenu-item"><a href="causes-single.html">Single cause </a></li>
                                 <li class="submenu-item"><a href="causes-single.html">Single cause </a></li>
                                 <li class="submenu-item"><a href="causes-single.html">Single cause </a></li>
                              </ul>

                            </li> -->
                           <!--  <li><a href="gallery.html">GALLERY</a></li>
                            <li><a href="contact.html">CONTACT</a></li> -->
                        </ul>
                    </div> <!-- /#navbar -->
                </div> <!-- /.container -->
            </div> <!-- /.navbar-main -->
        </nav> 
    </header> <!-- /. main-header -->   


    <!-- script -->

    <!-- jquery -->
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
    <!-- <script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"><\/script>')</script> -->


    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-migrate-3.0.0.min.js"></script>

    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <!-- Bootsrap javascript file -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="<?php echo base_url();?>assets/js/owl.carousel.js"></script>

     <!-- Template main javascript -->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>

    <!-- PrettyPhoto javascript file -->
    <script src="<?php echo base_url();?>assets/js/jquery.prettyPhoto.js"></script>

    <!-- helper -->    
    <script src="<?php echo base_url();?>assets/js/lazysizes.min.js"></script>

    <!-- Google map  -->
    <!-- <script src="http://maps.google.com/maps/api/js?sensor=false&amp;libraries=places" type="text/javascript"></script> -->

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
    

