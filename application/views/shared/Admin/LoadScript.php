<html>
	<head>
		<!-- Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Dosis:400,700' rel='stylesheet' type='text/css'>

		<!-- Font awesome -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">

		<!-- jquery ui -->
		<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css">		  -->
	
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/inspinia/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/inspinia/style.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/inspinia/animate.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/inspinia/font-awesome.min.css">

		<!-- Kendo -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/kendo/kendo.bootstrap-v4.min.css"> 


		<script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery-migrate-3.0.0.min.js"></script>

		<!-- Modernizr -->
		<script src="<?php echo base_url();?>assets/js/modernizr-2.6.2.min.js"></script>				

		<script src="<?php echo base_url();?>assets/js/inspinia/jquery-2.1.1.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/inspinia/inspinia.js"></script>
		<script src="<?php echo base_url();?>assets/js/inspinia/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/inspinia/plugins/metisMenu/metisMenu.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/inspinia/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
		
		<!-- kendo -->
		<script src="<?php echo base_url();?>assets/js/kendo/kendo.all.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/kendo/cultures/kendo.culture.id-ID.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/webapp.js"></script>

		
	</head>
</html>