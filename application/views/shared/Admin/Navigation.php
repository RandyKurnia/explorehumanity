<nav class="navbar-default navbar-static-side" role="navigation" id="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" src="<?php echo base_url(); ?>/assets/images/logo-white.png" />
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="#">Logout</a></li>
                        </ul>
                    </span>
                    
                </div>
                <div class="logo-element">
                    explore!
                </div>
            </li>

            <li class="">
                <a href="<?php echo base_url()?>admin/index" title="Dashboard"><i class="fa fa-lg fa-fw fa-dashboard"></i> <span class="nav-label">Dashboard</span></a>
            </li>          
            <li class="">
                <a href="<?php echo base_url()?>admin/donation" title="Donasi"><i class="fa fa-lg fa-fw fa-cart-arrow-down"></i> <span class="nav-label">Donasi</span></a>
            </li>     
            <li class="">
                <a href="<?php echo base_url()?>admin/donation" title="Donatur"><i class="fa fa-lg fa-fw fa-address-card-o"></i> <span class="nav-label">Donatur</span></a>
            </li>  
            <li class="">
                <a href="#" title="Data Master"><i class="fa fa-lg fa-fw fa-database"></i> <span class="nav-label" data-i18n="nav.master">Data Master</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class=""><a href="<?php echo base_url()?>admin/campaign" title="Campaign"><i class="fa fa-lg fa-fw fa-universal-access"></i><span class="nav-label">Campaign</span> </a></li>
                    <li class=""><a href="<?php echo base_url()?>admin/source" title="Sumber Info"><i class="fa fa-lg fa-fw fa-link"></i><span class="nav-label">Sumber Info</span> </a></li>
                    <li class=""><a href="<?php echo base_url()?>admin/bank" title="Rekening Bank"><i class="fa fa-lg fa-fw fa-briefcase"></i><span class="nav-label">Rekening Bank</span> </a></li>
                    <li class=""><a href="<?php echo base_url()?>admin/donaturcategory" title="Kategori Donatur"><i class="fa fa-lg fa-fw fa-id-badge"></i><span class="nav-label">Kategori Donatur</span> </a></li>
                    <li class=""><a href="<?php echo base_url()?>admin/campaigncategory" title="Kategori Campaign"><i class="fa fa-lg fa-fw fa-tags"></i><span class="nav-label">Kategori Campaign</span> </a></li>
                    <li class=""><a href="<?php echo base_url()?>admin/Faq" title="Faq"><i class="fa fa-lg fa-fw fa-question"></i><span class="nav-label">Faq</span> </a></li>
                </ul>
            </li>
            <li class="">
                <a href="#" title="User Management"><i class="fa fa-lg fa-fw fa-users"></i> <span class="nav-label" data-i18n="nav.master">User Management</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class=""><a href="<?php echo base_url()?>admin/role" title="Role / Akses"><i class="fa fa-lg fa-fw fa-unlock-alt"></i><span class="nav-label">Role / Akses</span> </a></li>
                    <li class=""><a href="<?php echo base_url()?>admin/user" title="User"><i class="fa fa-lg fa-fw fa-user-plus"></i><span class="nav-label">User</span> </a></li>
                </ul>
            </li>  
        </ul>
    </div>
</nav>
