<!-- section program -->
<div class="section-home our-causes fadeIn">
    <div class="container">
        <h2 class="title-style-1">Program Kami <span class="title-under"></span></h2>
        <div class="row">
            <?php foreach ($program as $key => $value) { 
                $namalink= preg_replace("/[^a-zA-Z0-9\s]/","", $value->Name);
                $namalink = str_replace(" ","-",$namalink);
                $namalink = strtolower($namalink);                          
            ?>  
             <div class="col-md-4 col-sm-6">
                <div class="cause">
                    <img src="<?php echo base_url();?>assets/images/photo/<?php echo $value->Photo; ?>" alt="<?php echo $value->Name; ?>" class="cause-img lazyload img-responsive">        
                    <h4 class="cause-title"><a href="#"><?php echo $value->Name;?> </a></h4>
                    <div class="cause-details">
                        <?php
                            if (strlen($value->Description) > 180) {
                                echo substr(strip_tags($value->Description), 0,180)."...";
                            }else{
                                echo $value->Description;
                            }
                        ?>
                    </div>
                    <div class="btn-holder text-center">
                      <a href="<?php echo base_url();?>donation/detail/<?php echo $namalink.'_'.$value->Id; ?>.html" class="btn btn-primary" style="width: 95%"> DONASI</a>                     
                    </div>
                </div> <!-- /.cause -->
            </div>       
            <?php } ?>            
        </div>
        <!-- <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary automatic-width" id="loadMoreBtn" >Load More</button>
            </div>
        </div> -->
    </div>
</div>