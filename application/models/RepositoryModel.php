<?php
class RepositoryModel extends CI_Model{
	public $table;

	public function GetAll($take, $skip, $sort_dir, $sort_field, $filterdata ) {
        
        // pengecekan apa ada trigger sort oleh user
        if( isset( $sort_dir ) ){
          
          // pengecekan apa ada trigger filter data oleh user
          if( $filterdata != 0 ){
            $this->db->order_by($sort_field, $sort_dir);
            $this->db->limit($take,$skip);

            // pengecekan filter operator kendo apa terset ?
            if( isset($filterdata['operator']) ) {
              
              // pengecekan filter operator default bawaaan kendo
              if( $filterdata['operator'] == 'eq' ) {
                $this->db->where($filterdata['field'], $filterdata['value']);
              }
              elseif( $filterdata['operator'] == 'neq' ) {
                $field = $filterdata['field'] . ' != ';
                $this->db->where($field, $filterdata['value']);
              }
              elseif( $filterdata['operator'] == 'startswith' ) {
                $this->db->like($filterdata['field'], $filterdata['value'], 'after');
              }
              elseif( $filterdata['operator'] == 'contains' ) {
                $this->db->like($filterdata['field'], $filterdata['value'], 'both');
              }
              elseif( $filterdata['operator'] == 'doesnotcontain' ) {
                $this->db->not_like($filterdata['field'], $filterdata['value']);
              }
              elseif( $filterdata['operator'] == 'endswith' ) {
                $this->db->like($filterdata['field'], $filterdata['value'],'before');
              }
            }

            $data = $this->db->get($this->table);
          }else{
            $this->db->order_by($sort_field, $sort_dir);
            $this->db->limit($take,$skip);
            $data = $this->db->get($this->table);
          }
          
        }else{
          
          if( $filterdata != 0 ){

            if( isset($filterdata['operator']) ) {
              if( $filterdata['operator'] == 'eq' ) {
                $this->db->where($filterdata['field'], $filterdata['value']);
              }
              elseif( $filterdata['operator'] == 'neq' ) {
                $field = $filterdata['field'] . ' != ';
                $this->db->where($field, $filterdata['value']);
              }
              elseif( $filterdata['operator'] == 'startswith' ) {
                $this->db->like($filterdata['field'], $filterdata['value'], 'after');
              }
              elseif( $filterdata['operator'] == 'contains' ) {
                $this->db->like($filterdata['field'], $filterdata['value'], 'both');
              }
              elseif( $filterdata['operator'] == 'doesnotcontain' ) {
                $this->db->not_like($filterdata['field'], $filterdata['value']);
              }
              elseif( $filterdata['operator'] == 'endswith' ) {
                $this->db->like($filterdata['field'], $filterdata['value'],'before');
              }
            }

            $this->db->limit($take,$skip);
            $data = $this->db->get($this->table);
          }else{
            $this->db->limit($take,$skip);
            $data = $this->db->get($this->table);
          }

        }
      
        return $data->result();
      }

    // menghitung total data table pada kendo
    public  function CountAll() {
        $count = $this->db->count_all($this->table);
        $query = $this->db->last_query();
        return $count;
    }

	  // menghitung total data jika terdapat kondisi tertentu pada kendo seperti filtering
    public function CountAllWhere($filterdata) {
        if( isset($filterdata['operator']) ) {
          if( $filterdata['operator'] == 'eq' ) {
            $this->db->where($filterdata['field'], $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'neq' ) {
            $field = $filterdata['field'] . ' !=';
            $this->db->where($field, $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'startswith' ) {
            $this->db->like($filterdata['field'], $filterdata['value'], 'after');
          }
          elseif( $filterdata['operator'] == 'contains' ) {
            $this->db->like($filterdata['field'], $filterdata['value'], 'both');
          }
          elseif( $filterdata['operator'] == 'doesnotcontain' ) {
            $this->db->not_like($filterdata['field'], $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'endswith' ) {
            $this->db->like($filterdata['field'], $filterdata['value'],'before');
          }
        }
        $this->db->from($this->table);
        $count = $this->db->count_all_results();
        return $count;
    }

    public function FindAll($param, $sort=NULL, $order=NULL)
    {
        if(is_null($sort) || is_null($order)) 
            $this->db->order_by('Id', 'DESC'); 
        else
            $this->db->order_by($sort, $order); 
     
        $query = $this->db->get_where($this->table, $param);        

        return $query->result();
    }
    
    public function Find($param, $sort=NULL, $order=NULL){
        if(is_null($sort) || is_null($order))   
            $this->db->order_by('Id', 'DESC'); 
        else
            $this->db->order_by($sort, $order); 
              
        $query = $this->db->get_where($this->table, $param);
        

        return $query->row();
    }
  
    public function FindById($id)
    {   
        $query = $this->db->get_where($this->table, array('id' => $id)); 
      
        return $query->row();
    }

    public function Save($data)
    {    
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function Update($data)
    {
        $this->db->update($this->table, $data);
    }

    public function Delete($id)
    {    
        $this->db->where('Id',$id);
        return $this->db->delete($table);
    }
	
}
