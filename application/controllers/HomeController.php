<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	function __Construct(){

        parent ::__construct();
    }

	public function Index()
	{
		
		$param = array('Status'=>1);
		$sort = 'OrderNumber';
		$order = 'ASC';

		$this->RepositoryModel->table = 'program';
		$data['program'] = $this->RepositoryModel->FindAll($param, $sort, $order);
		$this->load->view('Index',$data);
	}

	public function Profile()
	{
		$this->load->view('Profile');
	}

	public function Help()
	{
		$this->load->view('Help');
	}
}