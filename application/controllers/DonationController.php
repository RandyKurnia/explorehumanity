<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class DonationController extends CI_Controller {
	function __Construct(){

        parent ::__construct();	
    }

    //donation/detail
	public function Index($id)
	{			
		$programId = explode('.', $id)[0];
		$programId = explode('_', $id)[1];
		
		$param = array('Status'=>1);

		$this->RepositoryModel->table = 'program';
		$data['program'] = $this->RepositoryModel->FindById($programId);		

		$this->load->view('Donation/Index', $data);
	}

	//donation/program
	public function Donate($id)
	{			
		$programId = explode('.', $id)[0];
		$programId = explode('_', $id)[1];		

		$this->RepositoryModel->table = 'program';
		$data['program'] = $this->RepositoryModel->FindById($programId);
			
		$this->RepositoryModel->table = 'sourceinfo';
		$sort = 'Id';
		$order = 'ASC';
		$param = array('Status'=>1);
		$data['sourceInfo'] = $this->RepositoryModel->FindAll($param, $sort, $order);


		//edit bagian ini jika donasi bisa memilih rekening
		$this->RepositoryModel->table = 'programbank';
		$sort = 'Id';
		$order = 'ASC';
		$param = array('ProgramId'=>$programId);
		$data['bankAccount'] = $this->RepositoryModel->Find($param, $sort, $order);

		$this->load->view('Donation/Donate', $data);
	}

	//save donation
	public function Add(){
		$post = $this->input->post();	

		$now = date('Y-m-d H:i:s');
		$expiredDate = date('Y-m-d H:i:s',strtotime('+48 hour',strtotime($now)));

		$email = $post['Email'];
		$phone = $post['Phone'];
		$name = $post['Name'];

		$programId = $post['ProgramId'];
		$programName = $post['ProgramName'];
        $uniqueCode = $post['UniqueCode'];
        $amount = str_replace(",", "", $post['Amount']);
        $uniqueCode = $post['UniqueCode'];
        $sourceId = $post['SourceId'];
        $sourceOther = $post['SourceOther'];        
        $donationCategoryId = $post['DonationCategoryId'];
        $status = $post['Status'];
        $bankId = $post['BankId'];
        $total = $amount + $uniqueCode;

		$this->RepositoryModel->table = 'program';	
		$programModel = $this->RepositoryModel->FindById($programId);

		//ubah jika no rekening bisa lebih dari satu
		//mencari no rekening bank	
		$this->RepositoryModel->table = 'bankaccount';	
		$bankModel = $this->RepositoryModel->FindById($bankId);
		//

		$this->RepositoryModel->table = 'donatur';	
		$param = array('Email' => $email,'Phone'=>$phone, 'Name'=>$name);
		$donatur = $this->RepositoryModel->Find($param);

		if (!isset($donatur))
		{
			$donaturData = array(
								'Name' => $name,
								'Email' => $email,
								'Phone' => $phone
							);

			$donaturId = $this->RepositoryModel->Save($donaturData);
		}
		else
		{
			$donaturId = $donatur->Id;
		}

		$this->RepositoryModel->table = 'donation';	
		$donationData = array(
							'ProgramId' => $programId,
							'DonaturId' => $donaturId,
							'DonationCategoryId' => $donationCategoryId,
							'SourceId' => $sourceId,
							'DonationDate' => $now,
							'ExpireDate' => $expiredDate,
							'Amount' => $amount,
							'UniqueCode' => $uniqueCode,
							'Total' => $total,
							'SourceOther' => $sourceOther,
							'Status' => $status,
							'BankId' => $bankId
			 			);
		
		$donationId = $this->RepositoryModel->Save($donationData);
		$donationModel = $this->RepositoryModel->FindById($donationId);		
		
		$namalink= preg_replace("/[^a-zA-Z0-9\s]/","", $programName);
        $namalink = str_replace(" ","-",$namalink);
        $namalink = strtolower($namalink);

        $data['donatur'] = $donatur;
        $data['donation'] = $donationModel;
        $data['currentProgram'] = $programModel;
        $data['bank'] = $bankModel;
		$data['kirimEmail'] = $this->invoiceSend($data);


		$amount = number_format($donationModel->Total,0,".",".");	
		$date =  $donationModel->ExpireDate;	
		$expired = longdate_indo(date('Y-m-d',strtotime($date)))." ".date('H:i:s',strtotime($date));	

		$message="Terima Kasih, Anda telah berpartisipasi dalam program explore!, untuk DONASI silahkan TRANSFER tepat Rp. ".$amount." ke Rek. ".$bankModel->Bank." ".$bankModel->AccountNo." Cabang ".$bankModel->Branch." an. ".$bankModel->AccountName." Untuk informasi lebih lanjut silahkan hubungi/whatsapp 081229997667";
		
		$data['kirimEmail'] = $this->invoiceSend($data);
		//$data['sms'] = $this->SendSms($phone, $message);

		redirect(site_url('payment/'.$namalink.'_'.$donationId.'.html'));
	}

	public function Payment($id)
	{			
		$donationId = explode('.', $id)[0];
		$donationId = explode('_', $donationId)[1];				

		$this->RepositoryModel->table = 'donation';	
		$donation = $this->RepositoryModel->FindById($donationId);

		$this->RepositoryModel->table = 'donatur';	
		$donatur = $this->RepositoryModel->FindById($donation->DonaturId);

		$this->RepositoryModel->table = 'program';	
		$param = array('Status'=>1);
		$programAll = $this->RepositoryModel->FindAll($param);
		$program = $this->RepositoryModel->FindById($donation->ProgramId);
		
		//mencari no rekening bank
		$this->RepositoryModel->table = 'bankaccount';			
		$bank = $this->RepositoryModel->FindById($donation->BankId);

		$data['donatur'] = $donatur;
		$data['donation'] = $donation;
		$data['currentProgram'] = $program;
		$data['bank'] = $bank;
		$data['program'] = $programAll;		
		
		$this->load->view('Donation/Payment', $data);
	}

	public function invoiceSend($data){
		$config = array();
	    $config['charset'] = 'utf-8';
	    $config['useragent'] = 'Codeigniter';
	    $config['protocol']= "smtp";
	    $config['mailtype']= "html";
	    $config['smtp_host']= "ssl://mail.explorehumanity.id";//pengaturan smtp
		$config['smtp_port']= "465";
		$config['smtp_timeout']= "400";
	    $config['smtp_user']= "support@explorehumanity.id"; // isi dengan email kamu
	    $config['smtp_pass']= "bismillah25"; // isi dengan password kamu
	    //$config['smtp_pass']= "3xploreh1jau"; // isi dengan password kamu
	    $config['crlf']="\r\n"; 
	    $config['newline']="\r\n"; 
	    $config['wordwrap'] = TRUE;

	    $this->email->initialize($config);
	    $this->email->from($config['smtp_user'],"noreply@explorehumanity.id");
	    $this->email->to($data["donatur"]->Email);
	    $this->email->subject("Invoice Donasi");
	    $isi = $this->load->view('emailTamplate/invoiceDonasi',$data,true);
		$this->email->message($isi);
		$kirim = $this->email->send();
		return $kirim;	
	}

	public function SendSms($no, $message){
		// Script Kirim SMS Api Zenziva
		$userkey="1x2e6y"; // userkey lihat di zenziva
		$passkey="zk12jpdxus"; // set passkey di zenziva
		$url = "https://reguler.zenziva.net/apps/smsapi.php";

		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$no.'&pesan='.$message);
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		curl_setopt($curlHandle, CURLOPT_POST, 1);
		$results = curl_exec($curlHandle);
		

		if (!curl_errno($curlHandle)) {
		  switch ($http_code = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE)) {
		    case 200:  # OK
		    	$result = "OK";
		      break;
		    default:
		      $result = "Unexpected HTTP code: ". $http_code;
		  }
		}
		
		curl_close($curlHandle);
		return print $result;		
	}
}