<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DonationController extends CI_Controller {
	function __Construct(){
		parent ::__construct();
            
        if($this->session->userdata('login') != TRUE){
        	$this->session->set_userdata('redirect_back', current_url());  
            $url=base_url().'admin/login';
            redirect($url);
        }		    	 
	}

	
	public function Index(){
		$this->load->view('Admin/Donation/Index');
	}

	public function GetAll() {
    	// field untuk ditampilkan di depan
	    $this->RepositoryModel->table = "view_donationprogram";

	    // paginate handle
	    $take = $this->input->post('take');
	    $page = $this->input->post('page');
	    $skip = $this->input->post('skip');
	    $pageSize = $this->input->post('pageSize');

	    // sortir
	    $sort = $this->input->post('sort');
	    $sort_dir = $sort[0]['dir'];
	    $sort_field = $sort[0]['field'];

	    // filter
	    $filter = $this->input->post('filter');
	    if( $filter != '' ){
	      $filterdata = array();
	      $filterdata['field'] = $filter['filters'][0]['field'];
	      $filterdata['operator'] = $filter['filters'][0]['operator'];
	      $filterdata['value'] = $filter['filters'][0]['value'];
	      $filterdata['logic'] = $filter['logic'];  

	      $data = $this->RepositoryModel->GetAll($take, $skip, $sort_dir, $sort_field, $filterdata );
	      $total_data = $this->RepositoryModel->CountAllWhere($filterdata);
	    }else{
	      $filterdata = 0;
	      $data = $this->RepositoryModel->GetAll($take, $skip, $sort_dir, $sort_field, $filterdata );
	      $total_data = $this->RepositoryModel->CountAll();
	    }
	    
	    $return['Result'] = $data;
	    $return['Count'] = $total_data;

	    echo json_encode($return);
  	}

  	public function GetAllBank() {
    	// field untuk ditampilkan di depan
	    $this->RepositoryModel->table = "bankaccount";

	    // paginate handle
	    $take = $this->input->post('take');
	    $page = $this->input->post('page');
	    $skip = $this->input->post('skip');
	    $pageSize = $this->input->post('pageSize');

	    // sortir
	    $sort = $this->input->post('sort');
	    $sort_dir = $sort[0]['dir'];
	    $sort_field = $sort[0]['field'];

	    // filter
	    $filter = $this->input->post('filter');
	    if( $filter != '' ){
	      $filterdata = array();
	      $filterdata['field'] = $filter['filters'][0]['field'];
	      $filterdata['operator'] = $filter['filters'][0]['operator'];
	      $filterdata['value'] = $filter['filters'][0]['value'];
	      $filterdata['logic'] = $filter['logic'];  

	      $data = $this->RepositoryModel->GetAll($take, $skip, $sort_dir, $sort_field, $filterdata );
	      $total_data = $this->RepositoryModel->CountAllWhere($filterdata);
	    }else{
	      $filterdata = 0;
	      $data = $this->RepositoryModel->GetAll($take, $skip, $sort_dir, $sort_field, $filterdata );
	      $total_data = $this->RepositoryModel->CountAll();
	    }
	    
	    $return['Result'] = $data;
	    $return['Count'] = $total_data;

	    echo json_encode($return);
  	}
}