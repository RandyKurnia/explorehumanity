<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	function __Construct(){

        parent ::__construct();

    }

	public function Index()
	{
		if($this->session->userdata('login') != TRUE){
            $url=base_url().'admin/login';
            redirect($url);
        }
		else{
			if( $this->session->userdata('redirect_back') ) {
			    $redirect_url = $this->session->userdata('redirect_back');  // grab value and put into a temp variable so we unset the session value
			    $this->session->unset_userdata('redirect_back');
			    redirect( $redirect_url );
			}
			else
				$this->load->view('Admin/Index');
		}
	}

	public function Login()
	{		
		$this->load->view('Admin/Login');
	}

	public function Authentication()
	{
		$username = $this->input->post('Username',TRUE);
    	$password = $this->input->post('Password',TRUE);
		$encryptPass = do_hash($password, 'md5');

		$param = array('Status'=>1, 'Username'=>$username,'Password'=>$encryptPass);
		$this->RepositoryModel->table = 'user';
		$user = $this->RepositoryModel->Find($param);
		
		if (isset($user)){
			$this->SetSession($user);
			redirect(site_url('admin/index'));
		}
		else{			               	
	        $this->session->set_flashdata('message', 'Alamat email atau password salah');
        	redirect(site_url('admin/login'));
    	}

	}

	public function Logout(){
      $this->session->sess_destroy();
      redirect(site_url('admin/login'));
  	}

  	public function SetSession($data){
  		$userdata = array(
	        'username'  => $data->Username,
	        'phone'     => $data->Phone,
	        'fullname'  => $data->FullName,
	        'login' => TRUE
		);

		$this->session->set_userdata($userdata);
  	}

  	
}