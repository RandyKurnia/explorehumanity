<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'HomeController';
$route['404_override'] = 'e404';
$route['translate_uri_dashes'] = FALSE;

$route['help'] = 'HomeController/Help';
$route['profile'] = 'HomeController/Profile';

$route['donation/detail/(:any)'] = 'DonationController/Index/$1';
$route['donation/program/(:any)'] = 'DonationController/Donate/$1';
$route['donation/add'] = 'DonationController/Add';
$route['payment/(:any)'] = 'DonationController/Payment/$1';


// Area Administrator
$route['admin'] = 'Admin/HomeController/Index';
$route['admin/index'] = 'Admin/HomeController/Index';
$route['admin/login'] = 'Admin/HomeController/Login';
$route['admin/logout'] = 'Admin/HomeController/Logout';
$route['admin/authentication'] = 'Admin/HomeController/Authentication';

$route['admin/donation'] = 'Admin/DonationController/Index';
$route['admin/campaign'] = 'Admin/CampaignController/Index';
$route['admin/source'] = 'Admin/SourceController/Index';
$route['admin/bank'] = 'Admin/BankController/Index';
$route['admin/donaturcategory'] = 'Admin/DonaturCategoryController/Index';
$route['admin/campaigncategory'] = 'Admin/CampaignCategoryController/Index';
$route['admin/role'] = 'Admin/RoleController/Index';
$route['admin/user'] = 'Admin/UserController/Index';
$route['admin/faq'] = 'Admin/FaqController/Index';

$route['admin/donation/getall'] = 'Admin/DonationController/GetAll';
$route['admin/donation/getallbank'] = 'Admin/DonationController/GetAllBank';
$route['admin/campaign/getall'] = 'Admin/CampaignController/GetAll';
$route['admin/source/getall'] = 'Admin/SourceController/GetAll';
$route['admin/bank/getall'] = 'Admin/BankController/GetAll';
$route['admin/donaturcategory/getall'] = 'Admin/DonaturController/GetAll';
$route['admin/campaigncategory/getall'] = 'Admin/CampaignController/GetAll';