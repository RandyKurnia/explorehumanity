-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2018 at 03:19 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `explorehumanity`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankaccount`
--

CREATE TABLE `bankaccount` (
  `Id` int(11) NOT NULL,
  `Bank` varchar(200) NOT NULL,
  `AccountNo` varchar(50) NOT NULL,
  `AccountName` varchar(200) NOT NULL,
  `Branch` varchar(200) NOT NULL,
  `Code` int(10) NOT NULL,
  `Logo` varchar(100) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bankaccount`
--

INSERT INTO `bankaccount` (`Id`, `Bank`, `AccountNo`, `AccountName`, `Branch`, `Code`, `Logo`, `Status`) VALUES
(1, 'BNI Syariah', '600 006 6070', 'Explore Indonesia', 'Bandung', 427, 'bni-syariah.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

CREATE TABLE `donation` (
  `Id` int(11) NOT NULL,
  `ProgramId` int(11) DEFAULT NULL,
  `DonaturId` int(11) DEFAULT NULL,
  `DonationDate` datetime NOT NULL,
  `ExpireDate` datetime NOT NULL,
  `Amount` int(11) NOT NULL,
  `UniqueCode` int(11) NOT NULL,
  `Total` int(11) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donation`
--

INSERT INTO `donation` (`Id`, `ProgramId`, `DonaturId`, `DonationDate`, `ExpireDate`, `Amount`, `UniqueCode`, `Total`, `Status`) VALUES
(15, 3, 29, '2018-11-24 18:56:28', '0000-00-00 00:00:00', 1000000, 301, 1000301, 1),
(16, 3, 29, '2018-11-24 19:15:38', '2018-11-25 19:15:38', 1000000, 301, 1000301, 1),
(17, 3, 29, '2018-11-25 12:55:11', '2018-11-26 12:55:11', 1000000, 301, 1000301, 1);

-- --------------------------------------------------------

--
-- Table structure for table `donatur`
--

CREATE TABLE `donatur` (
  `Id` int(11) NOT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `Name` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Phone` varchar(20) NOT NULL,
  `Address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donatur`
--

INSERT INTO `donatur` (`Id`, `CategoryId`, `Name`, `Email`, `Phone`, `Address`) VALUES
(29, NULL, 'randy kurnia', 'ranz.kurnia@gmail.com', '085624301185', '');

-- --------------------------------------------------------

--
-- Table structure for table `donaturcategory`
--

CREATE TABLE `donaturcategory` (
  `Id` int(11) NOT NULL,
  `Name` varchar(150) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donaturcategory`
--

INSERT INTO `donaturcategory` (`Id`, `Name`, `Description`, `Status`) VALUES
(1, 'Perorangan', 'Donatur perorangan / pribadi', 1),
(2, 'Komunitas', 'Donatur komunitas / kelompok', 1),
(3, 'Instansi', 'Donatur Instansi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `Id` int(11) NOT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `BankAccountId` int(11) DEFAULT NULL,
  `UniqueCode` int(11) NOT NULL,
  `Name` varchar(500) NOT NULL,
  `TagLine` varchar(100) NOT NULL,
  `Description` text NOT NULL,
  `Photo` varchar(250) NOT NULL,
  `Banner` varchar(250) NOT NULL,
  `OrderNumber` int(11) NOT NULL,
  `StartDate` datetime DEFAULT NULL,
  `FinishDate` datetime DEFAULT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`Id`, `CategoryId`, `BankAccountId`, `UniqueCode`, `Name`, `TagLine`, `Description`, `Photo`, `Banner`, `OrderNumber`, `StartDate`, `FinishDate`, `Status`) VALUES
(1, 1, NULL, 0, 'Operasional', '', '', '', '', 0, NULL, NULL, 0),
(2, 2, 1, 500, 'Winter Project', 'Berbagi Kehangatan di Negeri Syam', '<b>Berbagi Kehangatan di Negeri Syam</b>\r\n<br><br>\r\nPara pengungsi Suriah sedang dihadapkan kepada kekhawatiran mereka menghadapi musim dingin yang akan segera tiba.\r\n<br><br>\r\nSuhu dingin ekstrim bisa mencapai dibawah 0° celcius.\r\n<br><br>\r\nTidak semua pengungsi tinggal di tempat-tempat yang layak atau di tenda-tenda yang bagus, bahkan ada tenda-tenda yang tambal sulam untuk menutupi lubang-lubang kerusakan tendanya.\r\n<br><br>\r\nGenangan air pun memasuki tenda-tenda mereka ketika salju turun.\r\n<br><br>\r\nBelum lagi kebutuhan akan stok makanan selama musim dingin nanti.\r\n<br><br>\r\nBegitu pula pakaian hangat, sepatu boots, selimut hangat, serta penghangat ruangan untuk tetap menghangatkan tubuh mereka.\r\n<br><br>\r\nAnak-anak menjadi yang paling rentan untuk terserang penyakit, seperti diare, batuk, hipotermia dan lain-lain sehingga dibutuhkan pula persediaan obat-obatan dan tenaga medis.\r\n<br><br>\r\nKurang lebih ada 7 juta pengungsi Suriah yang sangat ini membutuhkan perhatian dan bantuan kita.\r\n<br><br>\r\nKami membuka kesempatan dan mengajak kepada teman-teman untuk menjadi bagian yang ikut menghadirkan kehangatan bagi saudara-saudara pengungsi kita di Negeri Syam dalam program explore! Winter Project.', 'winter.jpeg', 'banner-winter.gif', 1, '2018-10-01 00:00:00', '2019-01-31 23:59:58', 1),
(3, 3, 1, 301, 'Food Basket', 'Keranjang Makanan untuk Negeri Syam', 'Program penyaluran bantuan berupa paket makanan dengan kecukupan gizi yang memadai untuk para korban yang berada di kamp-kamp pengungsian', 'foodbasket.jpeg', 'banner-food-basket.gif', 2, NULL, NULL, 1),
(4, 3, 1, 303, 'Shelter Kits for Sham', 'Make them feel at home', 'Yaitu Pengadaan peralatan-peralatan sehari-hari penunjang tenda-tenda pengungsian, seperti terpal, jerigen, lampu, matras, selimut, dan lain-lain', 'shelter.jpeg', 'banner-shelter-kits.gif', 3, NULL, NULL, 1),
(5, 7, 1, 402, 'Help Idlib', 'Help Idlib', '<b>Jutaan Anak-anak Idlib Dalam Bahaya</b><br><br>\r\n\r\nUNICEF memperingatkan bahwa ada 1 Juta anak-anak Suriah yang bisa menjadi korban apabila serangan terus terjadi di Idlib\r\n<br><br>\r\nSerangan militer di Idlib akan membuat hidup anak-anak terancam hak-hak dasarnya. \"Ribuan anak-anak di Idlib terpaksa meninggalkan rumah-rumah mereka demi menyelamatkan diri dan tinggal di tempat-tempat penampungan sementara yang sebenarnya tempat penampungan ini sudah penuh sesak juga oleh para pengungsi, belum lagi kebutuhan dasar mereka yang sulit terpenuhi, kebutuhan akan makanan, air bersih, obat-obatan yang sangat terbatas\". Kata Henrietta Fore, Direktur Eksekutif UNICEF.\r\n<br><br>\r\nGelombang serangan baru akan membuat mereka terjebak diantara garis serangan atau terperangkap di dalam target serangan itu sendiri sehingga mereka berada dalam situasi yang sangat berbahaya.', 'idlib.jpeg', 'banner-idlib.gif', 4, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `programbank`
--

CREATE TABLE `programbank` (
  `Id` int(11) NOT NULL,
  `ProgramId` int(11) DEFAULT NULL,
  `BankId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programbank`
--

INSERT INTO `programbank` (`Id`, `ProgramId`, `BankId`) VALUES
(3, 1, 1),
(4, 2, 1),
(5, 3, 1),
(6, 4, 1),
(10, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `programcategory`
--

CREATE TABLE `programcategory` (
  `Id` int(11) NOT NULL,
  `Name` varchar(500) NOT NULL,
  `TagLine` varchar(100) NOT NULL,
  `Description` text NOT NULL,
  `Photo` varchar(250) NOT NULL,
  `Banner` varchar(250) NOT NULL,
  `OrderNumber` int(11) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programcategory`
--

INSERT INTO `programcategory` (`Id`, `Name`, `TagLine`, `Description`, `Photo`, `Banner`, `OrderNumber`, `Status`) VALUES
(1, 'explore! Support', '', 'Program ini adalah program pengadaan bantuan operasional untuk menunjang dan mendukung program lainnya.', '', '', 0, 0),
(2, 'explore! Winter Project', '', 'Program ini adalah program reguler yang dijalankan setiap mendekati musim dingin yang sangat menyiksa bagi para pengungsi, bantuan berupa penghangat ruangan, selimut, makanan, dan lain lain yang sekiranya dapat membantu mengurangi penderitaan para pengungsi disana.', 'winter.jpeg', '', 1, 1),
(3, 'explore! Basic Needs', '', 'Program ini menitikberatkan penyaluran bantuan yang berhubungan dengan kebutuhan dasar manusia, seperti makanan, pakaian dan tempat tinggal.', 'basic-needs.jpeg', '', 2, 1),
(4, 'explore! Health Care', '', 'Program ini menitikberatkan pada penyaluran bantuan yang berhubungan dengan fasilitas kesehatan bagi para pengungsi di Syam (Palestina dan Suriah).', 'health-care.jpeg', '', 3, 1),
(5, 'explore! Qurban With Sham', '', 'Program ini adalah program tahunan setiap bulan zulhijah memperingati Hari Raya Idul Adha, Kita distribusikan daging Qurban sehingga pada moment Hari Raya ini saudara kita yang di pengungsian juga bisa merasakan nikmatnya santapan daging qurban sebagaimana kita nikmati disini.', 'qurban-sham.jpeg', '', 6, 1),
(6, 'Dapur Ramadhan', '', 'Program ini adalah program tahunan setiap bulan ramadhan, kita menyediakan makan sahur dan ifthor bagi muslimin yang berpuasa di pengungsian, program ini adalah program yang sangat penting pengingat karena dengan ini kita bisa menebarkan kebahagiaan untuk saudara kita yang berpuasa di tengah penderitaan akibat perang.', 'dapur-ramadhan.jpeg', '', 7, 1),
(7, 'Tanggap Bencana', '', 'Program ini adalah program insidental yang hanya dijalankan ketika terjadi bencana Alam atau bencana kemanusiaan yang mendesak untuk di beri bantuan, seperti gempa bumi, kebakaran, dan lain lain.', 'tanggap-bencana.jpeg', '', 5, 1),
(8, 'Peduli Anak Syam', '', 'Program ini menitikberatkan penyaluran bantuan yang berhubungan dengan kondisi anak-anak korban bencana kemanusiaan di sana. Mulai dari fasilitas belajar, penyediaan tenaga pengajar, gedung sekolah, tempat bermain dan lain lain', 'anak-syam.jpeg', '', 4, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bankaccount`
--
ALTER TABLE `bankaccount`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `donation`
--
ALTER TABLE `donation`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `DonationProgram` (`ProgramId`),
  ADD KEY `DonationDonatur` (`DonaturId`);

--
-- Indexes for table `donatur`
--
ALTER TABLE `donatur`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `DonaturCategory` (`CategoryId`);

--
-- Indexes for table `donaturcategory`
--
ALTER TABLE `donaturcategory`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `CategoryProgram` (`CategoryId`),
  ADD KEY `BankAccountProgram` (`BankAccountId`);

--
-- Indexes for table `programbank`
--
ALTER TABLE `programbank`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Program` (`ProgramId`),
  ADD KEY `Bank` (`BankId`);

--
-- Indexes for table `programcategory`
--
ALTER TABLE `programcategory`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bankaccount`
--
ALTER TABLE `bankaccount`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `donation`
--
ALTER TABLE `donation`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `donatur`
--
ALTER TABLE `donatur`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `donaturcategory`
--
ALTER TABLE `donaturcategory`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `programbank`
--
ALTER TABLE `programbank`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `programcategory`
--
ALTER TABLE `programcategory`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `donation`
--
ALTER TABLE `donation`
  ADD CONSTRAINT `DonationDonatur` FOREIGN KEY (`DonaturId`) REFERENCES `donatur` (`Id`) ON DELETE SET NULL,
  ADD CONSTRAINT `DonationProgram` FOREIGN KEY (`ProgramId`) REFERENCES `program` (`Id`) ON DELETE SET NULL;

--
-- Constraints for table `donatur`
--
ALTER TABLE `donatur`
  ADD CONSTRAINT `DonaturCategory` FOREIGN KEY (`CategoryId`) REFERENCES `donaturcategory` (`Id`) ON DELETE SET NULL;

--
-- Constraints for table `program`
--
ALTER TABLE `program`
  ADD CONSTRAINT `BankAccountProgram` FOREIGN KEY (`BankAccountId`) REFERENCES `bankaccount` (`Id`) ON DELETE SET NULL,
  ADD CONSTRAINT `CategoryProgram` FOREIGN KEY (`CategoryId`) REFERENCES `programcategory` (`Id`) ON DELETE SET NULL;

--
-- Constraints for table `programbank`
--
ALTER TABLE `programbank`
  ADD CONSTRAINT `Bank` FOREIGN KEY (`BankId`) REFERENCES `bankaccount` (`Id`),
  ADD CONSTRAINT `Program` FOREIGN KEY (`ProgramId`) REFERENCES `program` (`Id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
