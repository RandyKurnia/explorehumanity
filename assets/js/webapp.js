   
    var kendoGridFilterable = {
        messages: {
            info: "Tampilkan yang memiliki nilai:", // sets the text on top of the filter menu
            filter: "Filter", // sets the text for the "Filter" button
            clear: "Hapus", // sets the text for the "Clear" button

            // when filtering boolean numbers
            isTrue: "Benar", // sets the text for "isTrue" radio button
            isFalse: "Salah", // sets the text for "isFalse" radio button

            //changes the text of the "And" and "Or" of the filter menu
            and: "Dan",
            or: "Atau"
        },
        operators: {
            //string: {
            //    eq: "Is equal to",
            //    neq: "Is not equal to",
            //    startswith: "Starts with",
            //    contains: "Contains",
            //    endswith: "Ends with"
            //},
            //filter menu for "string" type columns
            string: {
                //eq: "Sama Dengan",
                //neq: "Tidak Sama Dengan",
                //startswith: "Memiliki Awalan",
                contains: "Memiliki Kata",
                //endswith: "Memiliki Akhiran"
            },
            //filter menu for "number" type columns
            number: {
                eq: "Sama Dengan",
                //neq: "Tidak Sama Dengan",
                //gte: "Lebih Besar Atau Sama Dengan",
                gt: "Lebih Besar",
                //lte: "Lebih Kecil Atau Sama Dengan",
                lt: "Lebih Kecil"
            },
            //filter menu for "date" type columns
            date: {
                eq: "Sama Dengan",
                //neq: "Tidak Sama Dengan",
                //gte: "Setelah Atau Sama Dengan",
                //gt: "Setelah",
                //lte: "Sebelum Atau Sama Dengan"
                //lt: "Sebelum"
            },
            //filter menu for foreign key values
            enums: {
                eq: "Sama Dengan",
                //neq: "Tidak Sama Dengan"
            }
        },
        extra: false
    };

    var kendoGridMessages = {
        noRecords: "Data Tidak Tersedia."
    };

    // var kendoGridPageable = {
    //     //numeric: false,
    //     //input: true,
    //     messages: pageableMessages
    // };


    var rangeDateFilterable = {
        ui: function (e) {
            var datePicker = $(e).kendoDatePicker({
                format: "dd/MM/yyyy",
                parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"]
            }).data("kendoDatePicker");

            datePicker.element.prop("readonly", "readonly");
        },
        operators: {
            date: {
                gte: "Setelah Atau Sama Dengan",
                lte: "Sebelum Atau Sama Dengan"
            }
        },
        extra: true
    };

    /**
     * @param {Function} uiOption function to init ui
     * @returns {kendo.ui.GridColumnFilterable} grid column filterable
     */
    var dropDownListFilterable = function (uiOption) {
        if (typeof (uiOption) !== "function") {
            throw "uiOption must be function";
        }

        return {
            ui: uiOption,
            operators: {
                string: {
                    eq: "Sama Dengan"
                },
                number: {
                    eq: "Sama Dengan"
                },
                enums: {
                    eq: "Sama Dengan"
                }
            },
            extra: false
        };
    };

// $(function(){
    //datepicker format
    kendo.culture("id-ID");    
    //datepicker format
    $(".form-control-datepicker").kendoDatePicker({
        format: "dd/MM/yyyy",
        parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"]
    }).prop("readonly", "readonly");

    $(".form-control-datetimepicker").kendoDateTimePicker({
        format: "dd/MM/yyyy HH:mm",
        parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"],
        interval: 30
    }).prop("readonly", "readonly");

    //numeric textbox
    $(".form-control-numeric").kendoNumericTextBox({
        //min: 0,
        //max: 2147483647,
        decimals: 0,
        format: "n0",
        spinners: false
    }).attr("type", "number");

    $(".form-control-decimal").kendoNumericTextBox({
        min: -2147483647,
        //max: 2147483647,
        decimals: 2,
        format: "n2",
        spinners: false
    });

    $(".form-control-decimal-n3").kendoNumericTextBox({
        //max: 2147483647,
        decimals: 3,
        format: "n3",
        spinners: false
    });

    $(".form-control-decimal-n4").kendoNumericTextBox({
        //max: 2147483647,
        decimals: 4,
        format: "n4",
        spinners: false
    });

    //kendo textarea
    $(".form-control-editor").kendoEditor({
        tools: [
            //{ name: "insertLineBreak", shift: false },
            //{ name: "insertParagraph", shift: true },
            "formatting",
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "justifyFull",
            "indent",
            "outdent",
            "createLink",
            "unlink",
            "viewHtml",
        ],
        resizable: true,
        encoded: false,
        paste: function (ev) {
            var temp = "";
            var pText = "";

            temp = $.parseHTML(ev.html);
            pText = $(temp).text();
            ev.html = pText;
        }
    });

    

    $(".numeric").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    function bankOptionFilter(element) {
        var url = "donation/getallbank";    
        var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        type: 'post',
                        url: url,   
                        dataType: 'json'
                    }
                },
                error: function(e) {
                    console.log(e);
                },
                schema: {
                    data: function(data){
                        return data.Result;
                    },
                    total: function(data){
                        return data.Count;
                    },
                    model: {
                        fields: {
                            Id:{ type: "number" },                            
                        }
                    },
                },                
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
        });

        element.kendoDropDownList({
            dataSource: ds,
            dataTextField: "Bank",
            dataValueField: "Id",
            optionLabel: "Pilih",
            filter:"startswith"
        });
    }
// });